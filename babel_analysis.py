from socket import TIPC_DEST_DROPPABLE
import statistics
from loguru import logger
import argparse
from transformers import BertTokenizer, BertModel
import torch
import matplotlib.pyplot as plt
import os
import numpy as np
import scipy.special
from bokeh.layouts import gridplot
from bokeh.plotting import figure, show
from gensim.models import KeyedVectors

import sys
import json

from trimesh import tol
sys.path.append('.')
from plot_utils import make_hist_bokeh, save_bokeh_plot, w2vec_test
from plot_utils import make_bar_bokeh, xy_plot_bokeh
from utils import read_json, write_json
import copy
from spellchecker import SpellChecker
from statistics import mean, median, stdev
from typing import List, Tuple
from constants import fix_spell

# everything at the first level should contain these keys (val, train sets)
keys_babel_d1 = {'babel_sid', 'url', 'feat_p', 'dur', 'seq_ann', 'frame_ann'}
keys_babel_d2_s = {'anntr_id', 'babel_lid', 'mul_act', 'labels'}
keys_babel_d2_f = {'anntr_id', 'babel_lid', 'mul_act', 'labels'}
EXCLUDED_ACTIONS = ['t-pose', 'a-pose', 'a pose','t pose', 
                    'tpose', 'apose', 'transition']
EXCLUDED_ACTIONS_WO_TR = ['t-pose', 'a-pose', 'a pose','t pose', 'tpose', 'apose']

# xx = seq_labels[0]['proc_label']

# tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
# model = BertModel.from_pretrained("bert-base-uncased")
# inputs = torch.tensor(tokenizer.encode(xx)).unsqueeze(0)
# outputs = model(inputs)
# print(outputs[0].shape)
# print(tokenizer.encode(xx), xx)
# https://babel-renders.s3.eu-central-1.amazonaws.com/

def t9(wds):
    spell = SpellChecker()
    wds = wds.strip().split()
    wds = [spell.correction(w) for w in wds]
    return ' '.join(wds)

def ratio_ab(a, b):
    if b==0:
        return 'Division by zero'
    x = a/b * 100
    return f'{round(x, 2)} % --> {a} / {b}'

def timeline_overlaps(arr1: Tuple, arr2: List[Tuple]) -> Tuple[List[Tuple], List[Tuple], List[Tuple]]:
    '''
    Returns the intervals for which:
    (1) arr1 has overlap with
    (2) arr1 is a subset of
    (3) arr1 is a superset of
    '''
    l = arr1[0]
    r = arr1[1]
    inter_sub = []
    inter_super = []
    inter_before = []
    inter_after = []
    for s in arr2:
        
        if (s[0] > l and s[0] > r) or (s[1] < l and s[1] < r):
            continue
        if s[0] <= l and s[1] >= r:
            inter_sub.append(s)
        if s[0] >= l and s[1] <= r:
            inter_super.append(s)
        if s[0] < l and s[1] < r and s[1] >= l:
            inter_before.append(s)
        if s[0] > l and s[0] <= r and s[1] > r:
            inter_after.append(s)

    return inter_before, inter_after
def segments_sorted(segs_fr: List[List], acts: List) -> Tuple[List[List], List]:

    assert len(segs_fr) == len(acts)
    if len(segs_fr) == 1: return segs_fr, acts
    L = [ (segs_fr[i],i) for i in range(len(segs_fr)) ]
    L.sort()
    sorted_segs_fr, permutation = zip(*L)
    sort_acts = [acts[i] for i in permutation]
    return  list(sorted_segs_fr), sort_acts

SMOOTH = 1e-6
def iou_numpy(outputs: np.array, labels: np.array):
    outputs = outputs.squeeze(1)
    
    intersection = (outputs & labels).sum((1, 2))
    union = (outputs | labels).sum((1, 2))
    
    iou = (intersection + SMOOTH) / (union + SMOOTH)
    
    thresholded = np.ceil(np.clip(20 * (iou - 0.5), 0, 10)) / 10
    
    return thresholded

def plot_timeline(seg_ids, seg_acts, outd, babel_id):
    import numpy as np
    import pylab as pl
    from matplotlib import collections  as mc
    from matplotlib.pyplot import cm
    import matplotlib.pyplot as plt
    seg_lns = [ [(x[0], i*0.01), (x[1], i*0.01)] for i, x in enumerate(seg_ids) ]
    colorline = cm.rainbow(np.linspace(0, 1, len(seg_acts)))
    lc = mc.LineCollection(seg_lns, colors=colorline, linewidths=3,
                            label=seg_acts)

    fig, ax = pl.subplots()

    ax.add_collection(lc)
    fig.tight_layout()
    ax.autoscale()
    ax.margins(0.1)
    # alternative for putting text there
    # from matplotlib.lines import Line2D
    # proxies = [ Line2D([0, 1], [0, 1], color=x) for x in colorline]
    # ax.legend(proxies, seg_acts, fontsize='x-small', loc='upper left')

    for i, a in enumerate(seg_acts):
        plt.text((seg_ids[i][0]+seg_ids[i][1])/2, i*0.01 - 0.002, a,
                 fontsize='x-small', ha='center')
    plt.title(f'Babel Sequence ID\n{babel_id}')
    plt.savefig(f'{outd}/plot_{babel_id}.png')

    plt.close()


def extract_frame_labels(babel_labels, fps, seqlen):
    pairs_from_transitions = 0
    pairs_not_from_transitions = 0

    seg_ids = []
    seg_acts = []
    data_out = {'sequence': None, 'segment': None, 'pairs': None}
    if babel_labels['frame_ann'] is None:

        # 'transl' 'pose''betas'
        action_label = babel_labels['seq_ann']['labels'][0]['proc_label']
        seg_ids.append([0, seqlen])
        seg_acts.append(fix_spell(action_label))
        data_out['sequence'] = [seg_acts, seg_ids]
    else:
        # Get segments
        for seg_an in babel_labels['frame_ann']['labels']:
            action_label = fix_spell(seg_an['proc_label'])

            st_f = int(seg_an['start_t']*fps)
            end_f = int(seg_an['end_t']*fps)
            if end_f > seqlen:
                end_f = seqlen
            seg_ids.append((st_f, end_f))
            seg_acts.append(action_label)
        # Process segments
        assert len(seg_ids) == len(seg_acts)
        import itertools

        seg_ids, seg_acts = segments_sorted(seg_ids, seg_acts)

        data_out['segment'] = [seg_acts, seg_ids]

        # remove a/t pose for pair calculation
        pairs_from_transitions = 0
        pairs_not_from_transitions = 0
        seg_acts_for_pairs = [a for a in seg_acts if a not in EXCLUDED_ACTIONS_WO_TR ]
        idx_to_keep = [i for i, a in enumerate(seg_acts) if a not in EXCLUDED_ACTIONS_WO_TR ]
        seg_ids_for_pairs = [s for i, s in enumerate(seg_ids) if i in idx_to_keep]

        # keep a/t pose to have absolute numbers
        # seg_acts_for_pairs = seg_acts[:]
        # seg_ids_for_pairs = seg_ids[:]

        # check data
        assert len(seg_acts_for_pairs) == len(seg_ids_for_pairs)

        seg2act = dict(zip(seg_ids_for_pairs, seg_acts_for_pairs))
        # plot_timeline(seg_ids, seg_acts, babel_key)
        
        overlaps_for_each_seg = {}
        for idx, segment in enumerate(seg_ids_for_pairs):
            # remove the segment of interest
            seg_ids_wo_seg = [x for x in seg_ids_for_pairs if x != segment]
            # calculate the before and after overlaps for the segment of interest
            ov_bef, ov_aft = timeline_overlaps(segment, seg_ids_wo_seg)

            overlaps_for_each_seg[segment] = {}
            overlaps_for_each_seg[segment]['before'] = ov_bef
            overlaps_for_each_seg[segment]['after'] = ov_aft

        pairs_s = []
        pairs_a = []
        for seg_, ov_seg in overlaps_for_each_seg.items():
            cur_act_pairs = []
            cur_seg_pairs = []
            cur_seg_pairs_bef = []
            cur_seg_pairs_af = []
            if  seg2act[seg_] == 'transition':
                # if transition is not the start
                if not seg_[0] == 0:
                    if ov_seg['before'] and ov_seg['after']:
                        cur_seg_pairs = list(itertools.product(ov_seg['before'], ov_seg['after']))
                        cur_act_pairs = [(seg2act[x], seg2act[y]) for x, y in cur_seg_pairs]

                        breakpoint()
                        cur_seg_pairs = [(min(min(a), min(b), min(seg_)),
                                        max(max(a), max(b), max(seg_))) for a, b in cur_seg_pairs]

                        pairs_s.append(cur_seg_pairs)
                        pairs_a.append(cur_act_pairs)
                if pairs_s:
                    pairs_from_transitions += len(cur_seg_pairs)
            else:
                ov_seg['before'] = [x for x in ov_seg['before'] if seg2act[x] != 'transition']
                ov_seg['after'] = [x for x in ov_seg['after'] if seg2act[x] != 'transition']
                if ov_seg['before']:
                    cur_seg_pairs_bef = list(itertools.product(ov_seg['before'], [seg_]))
                if ov_seg['after']:
                    cur_seg_pairs_af = list(itertools.product([seg_], ov_seg['after']))

                if ov_seg['after'] and ov_seg['before']:
                    cur_seg_pairs = cur_seg_pairs_bef + cur_seg_pairs_af
                elif ov_seg['after']:
                    cur_seg_pairs = cur_seg_pairs_af
                elif ov_seg['before']:
                    cur_seg_pairs = cur_seg_pairs_bef
                else:
                    continue
                breakpoint()

                cur_act_pairs = [(seg2act[x], seg2act[y]) for x, y in cur_seg_pairs]
                cur_seg_pairs = [(min(min(a), min(b)), max(max(a), max(b))) for a, b in cur_seg_pairs]

                pairs_s.append(cur_seg_pairs)
                pairs_a.append(cur_act_pairs)
                if pairs_s:
                    pairs_not_from_transitions += len(cur_seg_pairs)


        # flatten list of lists
        pairs_s = list(itertools.chain(*pairs_s))
        pairs_a = list(itertools.chain(*pairs_a))
        
        # remove duplicates
        from more_itertools import unique_everseen
        from operator import itemgetter

        tmp = zip(pairs_s, pairs_a)
        uniq_tmp = unique_everseen(tmp, key=itemgetter(0))
        segment_pairs = []
        action_pairs = []
        for seg, a in list(uniq_tmp):
            segment_pairs.append(seg)
            action_pairs.append(a)

        # conversion of actions to pair with comma
        action_pairs = [f'{a1}, {a2}' for a1, a2 in action_pairs]
        data_out['pairs'] = [action_pairs, segment_pairs]

    return data_out, pairs_from_transitions, pairs_not_from_transitions

def analyze_data(full, test, val, train, outd):

    nlp_analysis = True
    long_seq_analysis = False
    plot_w2vec = False
    hist_stats = False
    transition_analysis = False
    action_pairs_analysis = False
    length_analysis = False
    
    if length_analysis:
        seg_durations = []    

        st1_only = []
        seg_durations = []
        total_durs = []
        seg_durations_wo_transition_fr = []
        segs_transitions = []
        for k, s in full.items():
            seq_dur = s['dur']

            if s['frame_ann'] is not None:
                frame_labels = s['frame_ann']['labels']
                for a in frame_labels:
                    cur_seg_dur = a['end_t'] - a['start_t']
                    seg_durations.append(cur_seg_dur)

                    if a['proc_label'] not in EXCLUDED_ACTIONS:
                        seg_durations_wo_transition_fr.append(cur_seg_dur)
                    else:
                        segs_transitions.append(cur_seg_dur)
            else:
                st1_only.append(seq_dur)

            total_durs.append(seq_dur)
        for case, durs in zip(['Segments w/o Transitions, A/T pose', 'Segs', 'Seqs', 'All'],
                        [seg_durations_wo_transition_fr, seg_durations, st1_only, st1_only + seg_durations_wo_transition_fr]):
            # ll = [x[0] for x in durs]
            print(f'Data Type:{case}')
            mu = statistics.mean(durs)
            sigma = statistics.pstdev(durs)
            median = statistics.median(durs)
            print(f'μ:{round(mu, 2)} | σ:{round(sigma, 2)} | median:{round(median, 2)}')
            print('Number of samples:', len(durs))
        exit()
        hist, edges = np.histogram(ll, bins='auto')
        f1 = make_hist_bokeh(f'transitions distribution',
                            hist, edges, xaxis='x', yaxis='y',
                            save_path=f'{outd}/trans_hist.html')

        # HISTOGRAMS FOR STATISTICS AND DISTRIBUTIONS
        # without transitions
        hist, edges = np.histogram(seg_durations_wo_transition_fr, bins='auto')
        f1 = make_hist_bokeh(f'Durations Frame Segments wo Transitions[Full-Set]',
                            hist, edges, xaxis='durations', yaxis='# segs')
                        #   save_path=f'{outd}/plots/{split_name}_segs_only.html')

        # with transitions

        hist, edges = np.histogram(seg_durations, bins='auto')
        f3 = make_hist_bokeh(f'Durations Frame Segments[Full-Set]',
                            hist, edges, xaxis='durations', yaxis='# segs')
        hist, edges = np.histogram(st1_only, bins='auto')
        f4 = make_hist_bokeh(f'Durations seqs with St1 only[Full-Set]',
                            hist, edges, xaxis='durations', yaxis='# segs')    

        # put all the plots in a grid layout
        p1 = gridplot([[f1, f2], [f3, f4]])
        save_bokeh_plot(p1, f'{outd}/plots/all_MERGED.html')

    if action_pairs_analysis:
        fps = 30
        transitions_fr = []
        total_segs = 0
        datatype = 'pairs'
        ps_dur = []
        tot_tr_p = 0
        tot_not_tr_p = 0
        for k, s in full.items():
            total_frames = int(s['dur'] * 30)
            data_sample, tr_p, not_tr_p = extract_frame_labels(s, fps=30, seqlen=total_frames)
            if data_sample['sequence'] is not None:
                actions_seq, ids_seq = data_sample['sequence']

            if data_sample['segment'] is not None:
                actions_seg, ids_seg = data_sample['segment']
                actions_pairs, ids_pairs = data_sample['pairs']

                seg_durs = [(x[1] - x[0])/fps for x in ids_seg]
                seg_pair_durs = [(x[1] - x[0])/fps for x in ids_pairs]
                ps_dur.extend(seg_pair_durs)

            tot_tr_p += tr_p
            tot_not_tr_p += not_tr_p

        print(tot_tr_p, tot_not_tr_p)
        print(ratio_ab(tot_tr_p, tot_tr_p + tot_not_tr_p))
        print(ratio_ab(tot_not_tr_p, tot_tr_p + tot_not_tr_p))
        mu = statistics.mean(ps_dur)
        median = statistics.median(ps_dur)
        sigma = statistics.pstdev(ps_dur)
        print(f'μ:{mu} σ:{sigma} median:{median}')
        exit()
        hist, edges = np.histogram(ps_dur, bins='auto')
        f1 = make_hist_bokeh(f'Pairs of Action Distribution',
                            hist, edges, xaxis='x', yaxis='y',
                            save_path=f'{outd}/pairs_hist.html')

    if hist_stats:
        seg_durations = []    
        os.makedirs(outd, exist_ok=True)

        st1_only = []
        seg_durations = []
        total_durs = []
        seg_durations_wo_transition_fr = []
        seg_durations_wo_transition_seq = []
        sois = {}
        segs_transitions = []
        for k, s in full.items():
            seq_dur = s['dur']

            if s['frame_ann'] is not None:
                frame_labels = s['frame_ann']['labels']
                for a in frame_labels:
                    cur_seg_dur = a['end_t'] - a['start_t']
                    seg_durations.append(cur_seg_dur)
                    if k not in sois:
                        sois[k] = []
                    sois[k].append([cur_seg_dur, s['feat_p'], seq_dur,
                                    cur_seg_dur / seq_dur])

                    if a['proc_label'] != 'transition':
                        seg_durations_wo_transition_fr.append(cur_seg_dur)
                    else:
                        segs_transitions.append((cur_seg_dur, k))
            else:
                st1_only.append(seq_dur)
                seg_durations_wo_transition_seq.append(seq_dur)

            total_durs.append(seq_dur)
        ll = [x[0] for x in segs_transitions]
        mu = statistics.mean(ll)
        sigma = statistics.pstdev(ll)
        print(f'{mu} +/- {sigma}')

        hist, edges = np.histogram(ll, bins='auto')
        f1 = make_hist_bokeh(f'transitions distribution',
                            hist, edges, xaxis='x', yaxis='y',
                            save_path=f'{outd}/trans_hist.html')

        # Seqs for MOQUERY run it and look at this thing
        for k, v in sois.items():
            if len(v) > 1:
                c = all(x[3] < 0.6 for x in v)
                if c:
                    print(k, v[0][1])
        # HISTOGRAMS FOR STATISTICS AND DISTRIBUTIONS
        # without transitions
        hist, edges = np.histogram(seg_durations_wo_transition_fr, bins='auto')
        f1 = make_hist_bokeh(f'Durations Frame Segments wo Transitions[Full-Set]',
                            hist, edges, xaxis='durations', yaxis='# segs')
                        #   save_path=f'{outd}/plots/{split_name}_segs_only.html')

        hist, edges = np.histogram(seg_durations_wo_transition_seq, bins='auto')
        f2 = make_hist_bokeh(f'Durations seqs with St1 only wo Transitions[Ful-Set]',
                            hist, edges, xaxis='durations', yaxis='# segs')
                        #   save_path=f'{outd}/plots/{split_name}_STAGE1_only.html')
        # with transitions

        hist, edges = np.histogram(seg_durations, bins='auto')
        f3 = make_hist_bokeh(f'Durations Frame Segments[Full-Set]',
                            hist, edges, xaxis='durations', yaxis='# segs')
        hist, edges = np.histogram(st1_only, bins='auto')
        f4 = make_hist_bokeh(f'Durations seqs with St1 only[Full-Set]',
                            hist, edges, xaxis='durations', yaxis='# segs')    

        # put all the plots in a grid layout
        p1 = gridplot([[f1, f2], [f3, f4]])
        save_bokeh_plot(p1, f'{outd}/plots/all_MERGED.html')

    if nlp_analysis:

        # NLP ANALYSIS
        from utils import fix_spell
        import spacy
        nlp = spacy.load("en_core_web_trf") # en_core_web_sm
        # from transformers import BertTokenizer, 
        # from transformers import DistilBertTokenizer, DistilBertModel
        # tz_dbert = DistilBertTokenizer.from_pretrained("distilbert-base-uncased")
        # model = DistilBertModel.from_pretrained("distilbert-base-uncased")
        # tz_bert = BertTokenizer.from_pretrained("bert-base-cased")
        #tz.tokenize('walk in place')
        sents = []
        train_keys = list(babel_data_train.keys())
        val_keys = list(babel_data_val.keys())
        test_keys = list(babel_data_test.keys())
        pos_keep = ['VERB', 'ADJ', 'ADV', 'NOUN']
        k_of_wds = []
        for k, v in full.items():
            if v['frame_ann'] is not None:
                frame_labels = v['frame_ann']['labels']
                for a in frame_labels:
                    sents.append(fix_spell(a['proc_label']))
                    k_of_wds.append(k)
            else:
                seq_labels = v['seq_ann']['labels']
                if len(seq_labels) > 1:
                    exit('shit')
                for a in seq_labels:
                    sents.append(fix_spell(a['proc_label']))
                    k_of_wds.append(k)

        sents_tr = []
        sents_val = []
        sents_test = []
        for i, k in enumerate(k_of_wds):
            if k in train_keys:
                sents_tr.append(sents[i])
            elif k in val_keys:
                sents_val.append(sents[i])            
            elif k in test_keys:
                sents_test.append(sents[i])
        full = True
        if full:            
            splits = ['all']
            sents_lists = [sents]
        else:
            sents_lists = [sents_tr, sents_val, sents_test]
            splits = ['train', 'val', 'test', 'all']

        from collections import Counter
        v_all = []
        v_all_db = []

        processed_sents = []
        for split, sents_in_split in zip(splits, sents_lists):

            # For SpaCy
            vocab = []
            w_len = []

            # for DistillBERT
            vocab_distillbert = []
            w_len_db = []

            for s in sents_in_split:
                #bert_tokens = tz_dbert.tokenize(s)
                #w_len_db += [len(bert_tokens)]
                # vocab_distillbert.extend(bert_tokens)
 
                #for token in bert_tokens:
                #    tkn = nlp(token)
                #    for i in tkn: 
                #        if i.pos_ in pos_keep:
                #            vocab_distillbert.append(i.text)

                proc_sent = nlp(s)
                w_len += [len(proc_sent)]
                for token in proc_sent:
                    if token.pos_ in pos_keep:
                        vocab.append(token.text)
                processed_sents.append(proc_sent)
            import pickle
            with open('nlp-babel.pkl', 'wb') as handle:
                pickle.dump(processed_sents, handle, protocol=pickle.HIGHEST_PROTOCOL)
            continue
            for method, voc, sent_lens  in zip(['SpaCy', 'DistillBERT'],
                                               [vocab, vocab_distillbert],
                                               [w_len, w_len_db]):

                from statistics import mean, median, variance, stdev
                print(f'======== METHOD: {method} ========')
                print(f'Split Stats: {split.upper()}')
                print(f'μ:{round(mean(sent_lens), 2)} +/- {round(variance(sent_lens), 2)} Median: {round(median(sent_lens), 2)}')
                
                counts = dict(Counter(voc))
                freqs = sorted(counts.items(), key=lambda pair: pair[1], reverse=True)
                vocab = list(set(voc))  

                for fq_thres in [1000, 500, 250, 150, 100, 50, 10]:
                    print(f'More than {fq_thres} times:', sum(fq > fq_thres for wd, fq in freqs))
                print(f'Less than 10 times:', sum(fq < 10 for wd, fq in freqs))
                print(f'Less than 5 times:', sum(fq < 5 for wd, fq in freqs))
                make_bar_bokeh('Bar plot of Vocabulary', freqs, save_path=f'{outd}/vocabs_{split}.html')
                singletons = 0
                for x in freqs:
                    if x[1] == 1:
                        singletons += 1
                print(f'Singletons of Vocab({method}): {singletons}')
                print(f'Len of VOCABULARY: {len(vocab)}')
                
                write_json(vocab, f'./vocab-babel-{method}.json')
                write_json(freqs, f'./freqs-babel-{method}.json')



    if long_seq_analysis:
        
        # Long Sequence analysis
        dur_thresholds = np.linspace(10, 120, 100)
                
        long_ration = []
        seq_doms = []
        more_than_one_long = []
        actually_long = []
        for dur_threshold in dur_thresholds:
            no_of_longst2 = 0
            no_of_longst1 = 0
            tot_st1 = 0
            tot_st2 = 0
            label_long2 = {}
            label_long1 = []
            ds2 = []
            ds1 = []
            long_dict = []
            j = 0
            sents = []
            full_labels = []

            for k, v in full.items():
                dataset_name = v['feat_p'].strip().split('/')[0]

                if v['frame_ann'] is not None:
                    frame_labels = v['frame_ann']['labels']
                    first_time = True

                    for a in frame_labels:
                        tot_st2 += 1

                        cur_seg_dur = a['end_t'] - a['start_t']
                        if cur_seg_dur > dur_threshold:
                            if first_time:
                                label_long2[k] = []
                                first_time = False
                            no_of_longst2 += 1
                            label_long2[k].append([a['proc_label'],
                                                round(cur_seg_dur, 2),
                                                v['dur'],
                                                round(round(cur_seg_dur, 2) / v['dur'], 2)
                                                ])
                            sents.append(a['proc_label'])
                            ds2.append(dataset_name)
                        if a['proc_label']:
                            sents = []
                        full_labels.append(a['proc_label'])
                else:
                    tot_st1 += 1

                    seq_labels = v['seq_ann']['labels']
                    if v['dur'] > 15:
                        no_of_longst1 += 1
                        label_long1.append((seq_labels[0]['proc_label'], v['url']))
                        ds1.append(dataset_name)
                    full_labels.append(seq_labels[0]['proc_label'])
            
            se = 0
            more_than_big = 0
            for k, v in label_long2.items():
                if len(v) > 1:
                    more_than_big += 1
                for subs in v:
                    if subs[3] > 0.75:
                        se += 1
            long_ration += [(no_of_longst2 / tot_st2)*100]
            seq_doms += [(se / len(label_long2))*100]
            more_than_one_long += [(more_than_big / len(label_long2))*100]
        
        f1 = xy_plot_bokeh(dur_thresholds, long_ration, 
                    xaxis_label='Duration thresholds',
                    yaxis_label='Long frame segments(%)', 
                    w=600, h=600)
                    #save_path=f'{out_dir}/long_seqs.html')
        f2 = xy_plot_bokeh(dur_thresholds, seq_doms,
                    xaxis_label='Duration thresholds',
                    yaxis_label='Frame segments that comprise 0.8 of seq. (%)', 
                    w=600, h=600)
                    #   save_path=f'{out_dir}/seq_doms.html')
        f3 = xy_plot_bokeh(dur_thresholds, more_than_one_long,
                    xaxis_label='Duration thresholds',
                    yaxis_label='Sequences with >1 long segments(%)', 
                    w=600, h=600)
                    #   save_path=f'{out_dir}/more_than_one.html')
        p1 = gridplot([[f1, f2], [f3, None]])
        save_bokeh_plot(p1, f'{outd}/plots/stage2_long_stats.html')

        print(ratio_ab(no_of_longst2, tot_st2))
    if plot_w2vec:
    
        word_vpath = '/is/cluster/nathanasiou/data/GoogleNews-vectors-negative300.bin'
        kv_model = KeyedVectors.load_word2vec_format(word_vpath, binary=True)
        oov = []
        word_list = list(set(word_list))
        from spellchecker import SpellChecker
        spell = SpellChecker()
        spell.correction(single_w)
        import nltk
        from nltk.corpus import stopwords

        cachedStopWords = stopwords.words("english")
        real_words = []
        print('=====Started filtering word embeddings')
        for i, w in enumerate(word_list):
            words_of_phrase = w.strip().split()
            words_of_phrase = [spell.correction(single_w) for single_w in words_of_phrase]
            for sw in words_of_phrase:
                if sw in cachedStopWords:
                    continue
                if not kv_model.has_index_for(sw):
                    oov.append(sw)
                else:
                    real_words.append(' '.join(words_of_phrase))
        print('Finished filtering word embeddings======')
        
        w2vec_test(real_words, f'{outd}/embeddings.html')
        # print(oov)

        print(ratio_ab(len(oov), len(word_list)))

def check_dict_structure(bbl_d):
    
    for k, v in bbl_d.items():
        assert set(v.keys()) == keys_babel_d1, v.keys()
        assert set(v['seq_ann'].keys()) == keys_babel_d2_s
        if v['frame_ann'] is not None:
            assert set(v['frame_ann'].keys()) == keys_babel_d2_f

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--babel-path', required=True, type=str,
                        help='input path of AMASS data in unzipped format without anything else.')
    parser.add_argument('--out-path', required=True, type=str,
                        help='input path of AMASS data in unzipped format without anything else.')

    args = parser.parse_args()
    babel_dir = args.babel_path
    out_dir = args.out_path
    logger.info(f'Input arguments: \n {args}')
    
    babel_data_test = read_json(f'{babel_dir}/test.json')  
    babel_data_train = read_json(f'{babel_dir}/train.json')
    babel_data_val = read_json(f'{babel_dir}/val.json')
    babel_data_full = read_json(f'{babel_dir}/babel-full.json')

    check_dict_structure(babel_data_train)
    check_dict_structure(babel_data_test)
    check_dict_structure(babel_data_val)
    check_dict_structure(babel_data_full)

    db = analyze_data(babel_data_full, babel_data_test,
                      babel_data_val, babel_data_train, out_dir)
    '''
    python divotion/dataset/babel_analysis.py 
    --out-path ~/shared_logs/babel2.1-analysis 
    --babel-path /is/cluster/nathanasiou/data/babel/babel_v2.1
    '''
    # with open('/is/cluster/nathanasiou/data/amass/amass_cleanup_seqs/BMLrub.json', 'r') as f:
    #     bml_skate = json.load(f)

    # with open('/is/cluster/nathanasiou/data/amass/amass_cleanup_seqs/MPIHDM05.json', 'r') as f:
    #     mpi_skate = json.load(f)

    # seqs_bml = []
    # seqs_mpi = []
    # sq_a_mpi = 0
    # fr_a_mpi = 0
    # sq_a_bml = 0
    # fr_a_bml = 0
    # full_new = copy.deepcopy(bbl_f)

    # print(40*'=')
    # i = 0
    # from tqdm import tqdm
    # lll= {'treadmill_slow':('walk slowly in place', 'walk'),
    #       'treadmill_fast':('walk fast in place', 'walk'),
    #       'treadmill_norm':('walk in place', 'walk'),
    #       'normal_walk': ('walk in place', 'walk'),
    #       'treadmill_jog':('jog in place', 'jog'),
    #       'normal_jog':('jog in place', 'jog')}

    # for k, s  in full_new.items():
    #     p = s['feat_p']
    #     if p in mpi_skate:
    #         i+=1
    #         print(s['url'])
    #         print(p)
    #         print(40*'=')

    #         seqs_mpi.append(s['url'])
    #         if s['frame_ann'] is not None:
    #             print(s['frame_ann']['labels'])
    #             nl = input('Labels for current sequence:')
    #             nl = nl.split(',')

    #             new_annot = {}
    #             new_annot['act_cat'] = [nl[1]]
    #             new_annot['proc_label'] = nl[0]
    #             new_annot['raw_label'] = nl[0]
    #             new_annot['seg_id'] = s['frame_ann']['labels'][0]['seg_id']
    #             full_new[k]['frame_ann']['labels'] = []

    #             full_new[k]['frame_ann']['labels'].append(new_annot)
    #             fr_a_mpi += 1
    #             print(40*'=')

    #         else:
                
    #             print(s['seq_ann']['labels'])
    #             nl = input('Labels for current sequence:')
    #             nl = nl.split(',')

    #             new_annot = {}
    #             new_annot['act_cat'] = [nl[1]]
    #             new_annot['proc_label'] = nl[0]
    #             new_annot['raw_label'] = nl[0]
    #             new_annot['seg_id'] = s['seq_ann']['labels'][0]['seg_id']
    #             full_new[k]['seq_ann']['labels'] = []
    #             full_new[k]['seq_ann']['labels'].append(new_annot)

    #             sq_a_mpi += 1
    #             print(40*'=')

    #     if p in bml_skate:
    #         i+=1
    #         print(s['url'])
    #         print(p)
    #         print(40*'=')
    #         seqs_bml.append(s['url'])
    #         for fn, annot in lll.items():
    #             if fn in p:
    #                 nl = annot
    #                 found_it = True
    #                 break
    #         if s['frame_ann'] is not None:
    #             print(s['frame_ann']['labels'])
    #             new_annot = {}
    #             new_annot['act_cat'] = [nl[1]]
    #             new_annot['proc_label'] = nl[0]
    #             new_annot['raw_label'] = nl[0]
    #             new_annot['seg_id'] = s['frame_ann']['labels'][0]['seg_id']
    #             full_new[k]['frame_ann']['labels'] = []
    #             full_new[k]['frame_ann']['labels'].append(new_annot)

    #             fr_a_bml += 1
    #             print(40*'=')

    #         else:
    #             print(s['seq_ann']['labels'])

    #             new_annot = {}
    #             new_annot['act_cat'] = [nl[1]]
    #             new_annot['proc_label'] = nl[0]
    #             new_annot['raw_label'] = nl[0]
    #             new_annot['seg_id'] = s['seq_ann']['labels'][0]['seg_id']
    #             full_new[k]['seq_ann']['labels'] = []
    #             full_new[k]['seq_ann']['labels'].append(new_annot)
    #             print(40*'=')

    #             sq_a_bml += 1
    # write_json(full_new, 
    #            '/is/cluster/nathanasiou/data/babel/babel_v2.0/babel_full_v2.0.json')
    # exit()

