import pyrender
import trimesh
import numpy as np
import sys
import cv2
from smpl_utils import bodypart2color, marker2bodypart, c2rgba
import os
os.environ['PYOPENGL_PLATFORM'] = 'egl'

colors = {
        "blue": [0, 0, 255, 1],
        "cyan": [0, 255, 255, 1],
        "green": [0, 128, 0, 1],
        "yellow": [255, 255, 0, 1],
        "red": [255, 0, 0, 1],
        "grey": [77, 77, 77, 1],
        "black": [0, 0, 0, 1],
        "white": [255, 255, 255, 1],
        "transparent": [255, 255, 255, 0],
        "magenta": [197, 27, 125, 1],
        'pink': [197, 140, 133, 1],
        "light_grey": [217, 217, 217, 255],
        'yellow_pale': [226, 215, 132, 255],
        }

def get_checkerboard_plane(scale, loc, plane_width=4, num_boxes=9, center=True):
    
    ground_color = colors['light_grey']
    meshes = []
    
    ground = trimesh.primitives.Box(center=loc, extents=scale)

    # if center:
    #     c = c[0]+(pw/2)-(plane_width/2), c[1]+(pw/2)-(plane_width/2)
    # # trans = trimesh.transformations.scale_and_translate(scale=1, translate=[c[0], c[1], 0])
    ground.apply_translation([loc[0], loc[1], 0])
    # # ground.apply_transform(trimesh.transformations.rotation_matrix(np.rad2deg(-120), direction=[1,0,0]))
    # # ground.visual.face_colors = black if ((i+j) % 2) == 0 else white
    ground.visual.face_colors = ground_color
    meshes.append(ground)

    return meshes


class MeshViewer(object):

    def __init__(self, width=1200, height=800, add_ground_plane=True, 
                 ground_loc=None, ground_scale=None, use_offscreen=True,
                 bg_color='white'):
        super().__init__()

        self.use_offscreen = use_offscreen
        self.render_wireframe = False

        self.mat_constructor = pyrender.MetallicRoughnessMaterial
        self.trimesh_to_pymesh = pyrender.Mesh.from_trimesh

        self.scene = pyrender.Scene(bg_color=colors[bg_color],
                                    ambient_light=(0.3, 0.3, 0.3))

        pc = pyrender.PerspectiveCamera(yfov=np.pi / 3.0, aspectRatio=float(width) / height)
        camera_pose = np.eye(4)
        camera_pose[:3, 3] = np.array([0, 1.75, 3.25])
        self.camera_node = self.scene.add(pc, pose=camera_pose, name='pc-camera')

        self.figsize = (width, height)

        if add_ground_plane:
            ground_mesh = pyrender.Mesh.from_trimesh(get_checkerboard_plane(
                                                     ground_scale, ground_loc),
                                                     smooth=False)
            pose = trimesh.transformations.rotation_matrix(np.radians(90), [1, 0, 0])
            pose[:3, 3] = [0, -1, 0]
            self.scene.add(ground_mesh, pose=pose, name='ground_plane')

        if self.use_offscreen:
            self.viewer = pyrender.OffscreenRenderer(*self.figsize)
            self.use_raymond_lighting(5.)
        else:
            self.viewer = pyrender.Viewer(self.scene, use_raymond_lighting=True, viewport_size=self.figsize, cull_faces=False, run_in_thread=True)

    def set_background_color(self, color=colors['white']):
        self.scene.bg_color = color

    def update_camera_pose(self, camera_pose):
        self.scene.set_pose(self.camera_node, pose=camera_pose)

    def close_viewer(self):
        if self.viewer.is_active:
            self.viewer.close_external()

    def set_meshes(self, meshes, group_name='static', poses=[]):
        for node in self.scene.get_nodes():
            if node.name is not None and '%s-mesh'%group_name in node.name:
                self.scene.remove_node(node)

        if len(poses) < 1:
            for mid, mesh in enumerate(meshes):
                if isinstance(mesh, trimesh.Trimesh):
                    mesh = pyrender.Mesh.from_trimesh(mesh)
                self.scene.add(mesh, '%s-mesh-%2d'%(group_name, mid))
        else:
            for mid, iter_value in enumerate(zip(meshes, poses)):
                mesh, pose = iter_value
                if isinstance(mesh, trimesh.Trimesh):
                    mesh = pyrender.Mesh.from_trimesh(mesh)
                self.scene.add(mesh, '%s-mesh-%2d'%(group_name, mid), pose)

    def set_static_meshes(self, meshes, poses=[]): self.set_meshes(meshes, group_name='static', poses=poses)
    def set_dynamic_meshes(self, meshes, poses=[]): self.set_meshes(meshes, group_name='dynamic', poses=poses)

    def _add_raymond_light(self):
        from pyrender.light import DirectionalLight
        from pyrender.node import Node

        thetas = np.pi * np.array([1.0 / 6.0, 1.0 / 6.0, 1.0 / 6.0])
        phis = np.pi * np.array([0.0, 2.0 / 3.0, 4.0 / 3.0])

        nodes = []

        for phi, theta in zip(phis, thetas):
            xp = np.sin(theta) * np.cos(phi)
            yp = np.sin(theta) * np.sin(phi)
            zp = np.cos(theta)

            z = np.array([xp, yp, zp])
            z = z / np.linalg.norm(z)
            x = np.array([-z[1], z[0], 0.0])
            if np.linalg.norm(x) == 0:
                x = np.array([1.0, 0.0, 0.0])
            x = x / np.linalg.norm(x)
            y = np.cross(z, x)

            matrix = np.eye(4)
            matrix[:3, :3] = np.c_[x, y, z]
            nodes.append(Node(
                light=DirectionalLight(color=np.ones(3), intensity=1.0),
                matrix=matrix
            ))
        return nodes

    def use_raymond_lighting(self, intensity = 1.0):
        if not self.use_offscreen:
            sys.stderr.write('Interactive viewer already uses raymond lighting!\n')
            return
        for n in self._add_raymond_light():
            n.light.intensity = intensity / 3.0
            if not self.scene.has_node(n):
                self.scene.add_node(n)#, parent_node=pc)

    def render(self, render_wireframe=None, RGBA=False):
        from pyrender.constants import RenderFlags

        flags = RenderFlags.SHADOWS_DIRECTIONAL
        if RGBA: flags |=  RenderFlags.RGBA
        if render_wireframe is not None and render_wireframe==True:
            flags |= RenderFlags.ALL_WIREFRAME
        elif self.render_wireframe:
            flags |= RenderFlags.ALL_WIREFRAME
        color_img, depth_img = self.viewer.render(self.scene, flags=flags)

        return color_img


    def save_snapshot(self, fname):
        if not self.use_offscreen:
            sys.stderr.write('Currently saving snapshots only works with off-screen renderer!\n')
            return
        color_img = self.render()
        cv2.imwrite(fname, color_img)

import numpy as np
import torch
import trimesh
import pyrender
import math
import torch.nn.functional as F

def visualize_meshes(vertices, faces, pcd=None, multi_col=None,
                     multi_angle=False, h=480, w=480, bg_color='white',
                     save_path=None, fig_label=None):
    """[summary]

    Args:
        rec (torch.tensor): [description]
        inp (torch.tensor, optional): [description]. Defaults to None.
        multi_angle (bool, optional): Whether to use different angles. Defaults to False.

    Returns:
        np.array :   Shape of output (view_angles, seqlen, 3, im_width, im_height, )
 
    """
    im_height = h
    im_width = w
    vis_mar = True if pcd is not None else False

    # if multi_angle:
    #     view_angles = [0, 180, 90, -90]
    # else:
    #     view_angles = [0]
    seqlen = vertices.shape[0]
    kfs_viz = np.zeros((seqlen))
    if multi_col is not None:
        # assert len(set(multi_col)) == len(multi_col)
        if torch.is_tensor(multi_col):
            multi_col = multi_col.detach().cpu().numpy()
        elif not isinstance(multi_col, np.ndarray):
            multi_col = np.array(multi_col)
        kfs_viz.put(np.round_(multi_col).astype(int), 1, mode='clip')
    if isinstance(pcd, np.ndarray):
        pcd = torch.from_numpy(pcd)

    if vis_mar:
        pcd = pcd.reshape(seqlen, 67, 3).to('cpu')
        if len(pcd.shape) == 3:
            pcd = pcd.unsqueeze(0)
    mesh_rec = vertices
    mv = MeshViewer(width=im_width, height=im_height,
                    add_ground_plane=False, use_offscreen=True,
                    bg_color=bg_color)
    # ground plane has a bug if we want batch_size to work
    mv.render_wireframe = False
    video = np.zeros([seqlen, im_width, im_height, 3])
    for i in range(seqlen):
        Rx = trimesh.transformations.rotation_matrix(math.radians(-90), [1, 0, 0])
        Ry = trimesh.transformations.rotation_matrix(math.radians(90), [0, 1, 0])

        if vis_mar:
            m_pcd = trimesh.points.PointCloud(pcd[i])

        if kfs_viz[i]:
            mesh_color = np.tile(c2rgba(colors['light_grey']), (6890, 1))
        else:
            mesh_color = np.tile(c2rgba(colors['yellow_pale']), (6890, 1))

        m_rec = trimesh.Trimesh(vertices=mesh_rec[i],
                                faces=faces,
                                vertex_colors=mesh_color)

        m_rec.apply_transform(Rx)
        m_rec.apply_transform(Ry)

        all_meshes = []
        if vis_mar:
            m_pcd.apply_transform(Rx)
            m_pcd.apply_transform(Ry)

            m_pcd = np.array(m_pcd.vertices)
            # after trnaofrming poincloud visualize for each body part separately
            pcd_bodyparts = dict()
            for bp, ids in marker2bodypart.items():
                points = m_pcd[ids]
                tfs = np.tile(np.eye(4), (points.shape[0], 1, 1))
                tfs[:, :3, 3] = points
                col_sp = trimesh.creation.uv_sphere(radius=0.01)

                # debug markers, maybe delete it
                # if bp == 'special':
                #     col_sp = trimesh.creation.uv_sphere(radius=0.03)

                if kfs_viz[i]:
                    col_sp.visual.vertex_colors = c2rgba(colors["black"])
                else:
                    col_sp.visual.vertex_colors = c2rgba(colors[bodypart2color[bp]])

                pcd_bodyparts[bp] = pyrender.Mesh.from_trimesh(col_sp, poses=tfs)

            for bp, m_bp in pcd_bodyparts.items():
                all_meshes.append(m_bp)


        all_meshes.append(m_rec)

        mv.set_meshes(all_meshes, group_name='static')
        video[i] = mv.render()

    # if save_path is not None:
    #     save_video_samples(np.transpose(np.squeeze(video),
    #                                     (0, 3, 1, 2)).astype(np.uint8), 
    #                                     save_path, write_gif=True)
    #     return
    if multi_angle:
        return np.transpose(video, (0, 1, 4, 2, 3)).astype(np.uint8)

    if save_path is not None:
        return save_video_samples(np.transpose(np.squeeze(video),
                                               (0, 3, 1, 2)).astype(np.uint8),
                                  save_path, write_gif=False)
    return np.transpose(np.squeeze(video), (0, 3, 1, 2)).astype(np.uint8)

import os 
import cv2
from PIL import Image
import subprocess

def save_video_samples(vid_array, video_path, write_images=[], write_gif=True):

    if write_images:
        os.makedirs(video_path, exist_ok=True)
        for fid, img in enumerate(vid_array[write_images]):
            img_ar = np.uint8(np.transpose(img, (1, 2, 0)))
            img_f = Image.fromarray(img_ar)

            img_f.save(f'{video_path}/{fid}.png')
    video_folder_path = '/'.join(video_path.strip().split('/')[:-1])
    os.makedirs(video_folder_path, exist_ok=True)

    vid_path = f'{video_path}.mp4'
    gif_path = f'{video_path}.gif'
    # vid_array is (B, T, 3, W, H)
    w, h = vid_array.shape[-2], vid_array.shape[-1]
    fps = 30
    label = os.path.basename(video_path)
    print(label)
    if len(vid_array.shape) > 4:
        duration = vid_array.shape[1] // fps
        no_vids = vid_array.shape[0]
        # (B, T, 3, W, H) -> (T, B, W, H, 3)
        vid_array = np.transpose(vid_array,(1, 0, 3, 4, 2))

    else:
        duration = vid_array.shape[0] // fps
        no_vids = 1
        
    out = cv2.VideoWriter(vid_path, cv2.VideoWriter_fourcc(*'mp4v'),
                          fps, (no_vids*w, h)
                          )
    
    for img in vid_array:
        if len(vid_array.shape) > 4:
            img = np.uint8(np.hstack(img[:]))
            img_ar = img.copy()
            # img_ar = np.uint8(np.transpose(img.copy(), (1, 2, 0)))
            
        else:
            img_ar = np.uint8(np.transpose(img, (1, 2, 0)))
        cv2.putText(img_ar, f'{label}_gt_sample', (15,25),
                    cv2.FONT_HERSHEY_TRIPLEX, 0.7, (0,0,0), 1, cv2.LINE_AA)

        for i in range(1, no_vids+1):
            cv2.putText(img_ar, f'gen_{i}', (w*i + 10, 15),
                        cv2.FONT_HERSHEY_TRIPLEX, 0.7, (0,0,0), 1, cv2.LINE_AA)

        out.write(img_ar)
    out.release()
    if write_gif:
        cmd = ['ffmpeg', '-loglevel', 'quiet', '-y', '-i', vid_path, gif_path]
        subprocess.call(cmd)
    # assert retcode == 0, f"Command {' '.join(cmd)} failed, in function write_video_cmd."
    return video_path

