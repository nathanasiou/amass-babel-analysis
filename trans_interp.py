import sys
import argparse
from loguru import logger
import json
import glob
import joblib
from matplotlib.pyplot import axis
from tqdm import tqdm
import os
import torch
from geometry import axis_angle_to_quaternion, quaternion_to_axis_angle
from mesh_viz import save_video_samples
from utils import read_json, write_json, fix_spell
import numpy as np
from geometry import axis_angle_to_matrix
import functools
import operator
'''
RUN EXAMPLE
python divotion/dataset/add_babel_labels.py \
--input-path /is/cluster/nathanasiou/data/amass/processed_amass_smplh_wshape_30fps \ 
--out-path /is/cluster/nathanasiou/data/babel/babel-smplh30fps-gender \
--babel-path /is/cluster/nathanasiou/data/babel/babel_v2.1/
'''
def quat_slerp(x, y, a):
    """
    Performs spherical linear interpolation (SLERP) between x and y, with proportion a
    :param x: quaternion tensor
    :param y: quaternion tensor
    :param a: indicator (between 0 and 1) of completion of the interpolation.
    :return: tensor of interpolation results
    """
    
    len = np.sum(x * y, axis=-1)

    neg = len < 0.0
    len[neg] = -len[neg]
    y[neg] = -y[neg]

    a = np.zeros_like(x[..., 0]) + a
    amount0 = np.zeros(a.shape)
    amount1 = np.zeros(a.shape)

    linear = (1.0 - len) < 0.01
    omegas = np.arccos(len[~linear])
    sinoms = np.sin(omegas)

    amount0[linear] = 1.0 - a[linear]
    amount0[~linear] = np.sin((1.0 - a[~linear]) * omegas) / sinoms

    amount1[linear] = a[linear]
    amount1[~linear] = np.sin(a[~linear] * omegas) / sinoms
    res = amount0[..., np.newaxis] * x + amount1[..., np.newaxis] * y

    return res

def quat_normalize(x, eps=1e-8):
    """
    Normalizes a quaternion tensor
    :param x: data tensor
    :param eps: epsilon to prevent numerical instabilities
    :return: The normalized quaternions tensor
    """
    res = normalize(x, eps=eps)
    return res

def normalize(x, axis=-1, eps=1e-8):
    """
    Normalizes a tensor over some axis (axes)
    :param x: data tensor
    :param axis: axis(axes) along which to compute the norm
    :param eps: epsilon to prevent numerical instabilities
    :return: The normalized tensor
    """
    lgth = np.sqrt(np.sum(x * x, axis=axis, keepdims=True))
    res = x / (lgth + eps)
    return res

def interpolate_track(s, e, poses):
    # interpolation

    # extract the last good info
    lastgoodinfo = poses[s]

    # extract the first regood info
    newfirstgoodinfo = poses[e]

    q0 = axis_angle_to_quaternion(lastgoodinfo.reshape(22, 3))
    q1 = axis_angle_to_quaternion(newfirstgoodinfo.reshape(22, 3))

    # SLERP Local Quats:
    interp_ws = np.linspace(0.0, 1.0, num= e - (s + 1), dtype=np.float32)
    inter = np.stack([(quat_normalize(quat_slerp(quat_normalize(q0.numpy()), 
                                                 quat_normalize(q1.numpy()),
                                                 w))) for w in interp_ws],
                     axis=0)

    return quaternion_to_axis_angle(torch.from_numpy(inter)).numpy()

def slice_or_none(data, cslice):
    if data is None:
        return data
    else:
        return data[cslice]

def extract_frame_labels(babel_labels, fps, seqlen):

    seg_ids = []
    seg_acts = []
    if babel_labels['frame_ann'] is None:
        return None
    else:
        frame_labels = babel_labels['frame_ann']['labels']
        seg_lbsl = [x['proc_label'] for x in frame_labels]
        if 'transition' not in seg_lbsl:
            return None
        for seg_an in frame_labels:
            action_label = fix_spell(seg_an['proc_label'])

            st_f = int(seg_an['start_t']*fps)
            end_f = int(seg_an['end_t']*fps)
            if end_f > seqlen:
                end_f = seqlen
            seg_ids.append([st_f, end_f])
            seg_acts.append(action_label)

    return seg_ids, seg_acts

def process_data(input_dir, out_dir, amass2babel, babel_data_train,
                 babel_data_val, type_of_annots):
    
    amass_subsets = glob.glob(f'{input_dir}/*/*')
    
    for am_s_path in amass_subsets:
        amass_subset = joblib.load(am_s_path)
        logger.info(f'Loading the dataset from {am_s_path}')
        import random
        lofs = random.choices(amass_subset, k=int(0.2*len(amass_subset)))
        for sample in tqdm(amass_subset):
            name_for_data = sample['fname'].split('/')[0]
            if sample['fname'] not in amass2babel:
                continue
            split_of_seq = amass2babel[sample['fname']]['split']
            babel_seq_id = amass2babel[sample['fname']]['babel_id']

            if split_of_seq == 'train':
                babel_data_seq = babel_data_train[babel_seq_id]
            elif split_of_seq == 'val':
                babel_data_seq = babel_data_val[babel_seq_id]
            elif split_of_seq == 'test':
                continue

            r = extract_frame_labels(babel_data_seq, sample['fps'], sample['poses'].shape[0])
            if r is not None:
                seg_indices, seg_actions = r
            else:
                continue
            batch_size = 512

            poses = torch.from_numpy(sample['poses']).float()
            trans = torch.from_numpy(sample['trans']).float()
            nposes = poses.shape[0]
            from smplx.body_models import SMPLHLayer
            from mesh_viz import visualize_meshes

            import contextlib

            # Remove annoying print
            with contextlib.redirect_stdout(None):
                smplh = SMPLHLayer('/is/cluster/nathanasiou/data/smpl_models/smplh',
                                   ext="pkl", gender='male').eval()

            faces = smplh.faces

            tr_ids = []
            for ids, act in zip(seg_indices, seg_actions):
                if act == 'transition':
                    tr_ids.append(ids)
            poses_interp = poses.reshape(nposes, 52, 3)[:, :22]
            for p in tr_ids:
                i, j = p
                if j - i >= 2:
                    if i==0:
                        x = interpolate_track(i, j, poses_interp)
                    else:
                        x = interpolate_track(i, j, poses_interp)
                    # x = [i[np.newaxis, :] for i in x]
                    poses_interp[(i+1):j] = torch.from_numpy(x).float()
                else:
                    poses_interp[i:j] = poses.reshape(nposes, 52, 3)[:, :22][i:j]
            if not tr_ids:
                continue
            if len(tr_ids) < 2:
                continue
            
            matrix_poses_interp = axis_angle_to_matrix(poses_interp)

            # Reshaping
            matrix_poses_interp = matrix_poses_interp.reshape((nposes, *matrix_poses_interp.shape[-3:]))
            global_orient_interp = matrix_poses_interp[:, 0]

            trans_all = trans.reshape((nposes, 3))

            body_pose_interp = matrix_poses_interp[:, 1:22]
            left_hand_pose = None
            right_hand_pose = None
            n = len(body_pose_interp)
            outputs = []
            for chunk in range(int((n - 1) / batch_size) + 1):
                chunk_slice = slice(chunk * batch_size, (chunk + 1) * batch_size)
                smpl_output = smplh(global_orient=slice_or_none(global_orient_interp, chunk_slice),
                                        body_pose=slice_or_none(body_pose_interp, chunk_slice),
                                        left_hand_pose=slice_or_none(left_hand_pose, chunk_slice),
                                        right_hand_pose=slice_or_none(right_hand_pose, chunk_slice),
                                        transl=slice_or_none(trans_all, chunk_slice))

                output_chunk = smpl_output.vertices
                outputs.append(output_chunk)


            outputs = torch.cat(outputs)
            outputs_interp = outputs.reshape((nposes, *outputs.shape[1:]))
            tr_ids = [ list(np.arange(i,j+1)) for i,j in tr_ids ]
            tr_ids = functools.reduce(operator.concat, tr_ids)
            
            v = visualize_meshes(outputs_interp, faces, multi_col=tr_ids)
            save_video_samples(v, f'/home/nathanasiou/Dropbox/Projects/TEACH/transitions/{name_for_data}/{babel_seq_id}_interpolated')
      
            save_shape_bs_len = 1
            poses_gt = axis_angle_to_matrix(torch.from_numpy(sample['poses']).float().reshape(nposes, 52, 3)[:, :22])

            # Convert any rotations to matrix
            # from temos.tools.easyconvert import to_matrix
            # matrix_poses = to_matrix(input_pose_rep, poses)
            matrix_poses_gt = poses_gt

            # Reshaping
            matrix_poses_gt = matrix_poses_gt.reshape((nposes, *matrix_poses_gt.shape[-3:]))
            global_orient = matrix_poses_gt[:, 0]
            if trans is None:
                trans = torch.zeros((*save_shape_bs_len,  3), dtype=poses.dtype, device=poses.device)

            trans_all = trans.reshape((nposes, 3))

            body_pose = matrix_poses_gt[:, 1:22]
            left_hand_pose = None
            right_hand_pose = None
            n = len(body_pose)
            outputs = []
            for chunk in range(int((n - 1) / batch_size) + 1):
                chunk_slice = slice(chunk * batch_size, (chunk + 1) * batch_size)
                smpl_output = smplh(global_orient=slice_or_none(global_orient, chunk_slice),
                                        body_pose=slice_or_none(body_pose, chunk_slice),
                                        left_hand_pose=slice_or_none(left_hand_pose, chunk_slice),
                                        right_hand_pose=slice_or_none(right_hand_pose, chunk_slice),
                                        transl=slice_or_none(trans_all, chunk_slice))

                output_chunk = smpl_output.vertices
                outputs.append(output_chunk)

            outputs = torch.cat(outputs)
            outputs = outputs.reshape((nposes, *outputs.shape[1:]))
            v = visualize_meshes(outputs, faces, multi_col=tr_ids)
            save_video_samples(v, f'/home/nathanasiou/Dropbox/Projects/TEACH/transitions/{name_for_data}/{babel_seq_id}')
            continue
            for index, seg in enumerate(seg_indices):
                
                sample_babel = {}
                sample_babel['fps'] = sample['fps']
                sample_babel['fname'] = sample['fname']
                for ams_k in ['poses', 'trans', 'joint_positions', 'markers']:
                    sample_babel[ams_k] = sample[ams_k][seg[0]:seg[1]]
                
                sample_babel['action'] = seg_actions[index]
    exit()
    os.makedirs(out_dir, exist_ok=True)
    for k, v in dataset_db_lists.items():
        joblib.dump(v, f'{out_dir}/{k}.pth.tar')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--input-path', required=True, type=str,
                        help='input path of AMASS data in unzipped format without anything else.')
    parser.add_argument('--babel-path', required=True, type=str,
                        help='input path of AMASS data in unzipped format without anything else.')
    parser.add_argument('--out-path', required=True, type=str,
                        help='input path of AMASS data in unzipped format without anything else.')
    parser.add_argument('--annot', choices=['seq','seg','full'], required=True,
                        help='input path of AMASS data in unzipped format without anything else.')

    args = parser.parse_args()
    input_dir = args.input_path
    babel_dir = args.babel_path
    out_dir = args.out_path
    type_of_annots = args.annot
    logger.info(f'Input arguments: \n {args}')

    babel_data_train = read_json(f'{babel_dir}/train.json')
    babel_data_val = read_json(f'{babel_dir}/val.json')
    babel_data_test = read_json(f'{babel_dir}/test.json')

    amass2babel = read_json(f'{babel_dir}/id2fname/amass-path2babel.json')

    db = process_data(input_dir, out_dir, amass2babel,
                      babel_data_train, babel_data_val, type_of_annots)
