import json

def get_babel_render(k):
    k2s = str(k).zfill(6)
    return f'https://babel-renders.s3.eu-central-1.amazonaws.com/{k2s}.mp4'

def read_json(p):
    with open(p, 'r') as fp:
        json_contents = json.load(fp)
    return json_contents

def write_json(data, p):
    with open(p, 'w') as fp:
        json.dump(data, fp, indent=2)

# aspell list < your_file.txt > mispell2
from constants import SPELL_CORRECTOR

def fix_spell(words):
    l_words = words.strip().split()
    for i, x in enumerate(l_words):
        if x in SPELL_CORRECTOR:
            l_words[i] = SPELL_CORRECTOR[x]
    return ' '.join(l_words)
