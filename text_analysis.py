import argparse
import pickle
import spacy
from loguru import logger
from collections import Counter
from tqdm import tqdm

nlp = spacy.load("en_core_web_trf") # en_core_web_sm
POS = ['VERB', 'ADV', 'NOUN', 'ADJ']

babel_p = '/is/cluster/work/nathanasiou/data/motion-language/babel/babel_v2.1/nlp-babel.pkl'
kit_p = '/is/cluster/work/nathanasiou/data/motion-language/nlp-kit.pkl'

def analyze_text(texts_kit, texts_babel):
    for dataset, texts in zip(['KIT', 'BABEL'], [texts_kit, texts_babel]):
        print(f'Analyzing {dataset}')
        vocab = []
        all_words = []
        for lang_label in tqdm(texts):
            for tkn in lang_label:
                if tkn.lemma_ not in vocab and tkn.pos_ in POS:
                    vocab.append(tkn.lemma_)
                all_words.append(tkn.lemma_)
    
        print("Vocabulary length:", len(vocab))
        counts = dict(Counter(all_words))
        freqs = sorted(counts.items(), key=lambda pair: pair[1], reverse=True)
        for fq_thres in [1000, 500, 250, 150, 100, 50, 10, 5]:
            print(f'More than {fq_thres} times:', sum(fq > fq_thres for wd, fq in freqs))
        print(f'Singletons:', sum(fq == 1 for wd, fq in freqs))

if __name__ == '__main__':

    
    with open(babel_p, 'rb') as f:
        texts_babel = pickle.load(f)
    with open(kit_p, 'rb') as f:
        texts_kit = pickle.load(f)
    db = analyze_text(texts_kit, texts_babel)
