import os
from tkinter.font import names

from bokeh.plotting import figure, output_file, save
from bokeh.models import ColumnDataSource
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import time
from gensim.models import KeyedVectors
import numpy as np
import os
import matplotlib.pyplot as plt
# Install this for it to work: sudo apt install firefox-geckodriver
from bokeh.io import export_png
from typing import List, Optional, Tuple
from bokeh.models.sources import ColumnarDataSource
BOKEH_TOOLS = "pan,wheel_zoom,box_zoom,reset,hover,save"

def save_bokeh_plot(fig, fname):
    os.makedirs(os.path.dirname(fname), exist_ok=True)

    output_file(filename=fname)
    save(fig, filename=fname)

def make_bar_bokeh(title: str, data: List[Tuple], xaxis: Optional[str] = '',
                   yaxis: Optional[str] = '', save_path=None):

    # output_file("bar_sorted.html")

    # sorting the bars means sorting the range factors
    sorted_data = sorted(data, key=lambda tup: tup[1], reverse=True)

    datanames = [x[0] for x in sorted_data]
    counts =  [x[1] for x in sorted_data]
    data = ColumnDataSource(data=dict(words=datanames, counts=counts))
    TOOLTIPS = [
    ("index", "$index"),
    ("freq", "@counts"),
    ("desc", "@words"),
]
    p = figure(title=title, tools=BOKEH_TOOLS, background_fill_color="#fafafa",
               x_range=datanames, width=2000, height=1200, tooltips=TOOLTIPS)
    # p.vbar(x=datanames, top=counts)

    p.vbar(top='counts', x='words', source=data)
    p.xaxis.axis_label = xaxis
    p.yaxis.axis_label = yaxis
    p.grid.grid_line_color="white"
    if save_path is not None:
        if os.path.isdir(save_path):
            os.makedirs(os.path.dirname(save_path), exist_ok=True)
        # set output to static HTML file
        if '.html' in save_path:
            save_bokeh_plot(p, save_path)
        elif '.png' in save_path:
            export_png(p, filename=save_path)
    return p

def make_hist_bokeh(title, hist, edges, xaxis, yaxis, save_path=None):
    p = figure(title=title,
               tools=BOKEH_TOOLS,
               background_fill_color="#fafafa")
    p.quad(top=hist, bottom=0, left=edges[:-1], right=edges[1:],
        fill_color="navy", line_color="white", alpha=0.5)
    # p.line(x, pdf, line_color="#ff8888", line_width=4, alpha=0.7, legend_label="PDF")
    # p.line(x, cdf, line_color="orange", line_width=2, alpha=0.7, legend_label="CDF")
    p.y_range.start = 0
    # p.legend.location = "center_right"
    # p.legend.background_fill_color = "#fefefe"
    p.xaxis.axis_label = xaxis
    p.xaxis.ticker = edges
    p.yaxis.axis_label = yaxis
    p.grid.grid_line_color="white"
    if save_path is not None:
        if os.path.isdir(save_path):
            os.makedirs(os.path.dirname(save_path), exist_ok=True)
        # set output to static HTML file
        if '.html' in save_path:
            save_bokeh_plot(p, save_path)
        elif '.png' in save_path:
            export_png(p, filename=save_path)
    return p

def xy_plot_bokeh(x, y, xaxis_label, yaxis_label, line_width=2,
                  w=400, h=400, save_path=None):
    # add a line renderer
    ttootps = [("index", "$index"),
               ("(x,y)", "($x, $y)")]
    p = figure(width=w, height=h, tools=BOKEH_TOOLS, tooltips=ttootps)
    p.line(x, y, line_width=line_width)
    p.circle(x, y, radius=0.5, alpha=0.3, color='darkred')
    p.xaxis.axis_label = xaxis_label
    p.yaxis.axis_label = yaxis_label
    if save_path is not None:
        os.makedirs(os.path.dirname(save_path), exist_ok=True)
        # set output to static HTML file
        save_bokeh_plot(p, save_path)
    return p

def pca_tsne(features):
    """Performs first pca then tsne on the features
        features: [N, D]
    """
    pca_features = pca(features, pca_comps=50)
    tsne_features = tsne(pca_features, perplexity=30)
    return tsne_features

def pca(features, pca_comps=50):
    """Performs pca on the features
        features: [N, D]
    """
    begin = time.time()
    pca = PCA(n_components=pca_comps, svd_solver="full")
    pca.fit(features)
    end = time.time()
    print(f"Finished pca in {end - begin:.3f} seconds")
    # print(f"explained var: {pca.explained_variance_ratio_}")
    reduced_features = pca.transform(features)
    return reduced_features


def tsne(features, perplexity=30):
    """Performs tsne on the features
        features: [N, D]
    """
    begin = time.time()
    features_embedded = TSNE(n_components=2, perplexity=perplexity).fit_transform(
        features
    )
    end = time.time()
    print(f"Finished tsne in {end - begin:.3f} seconds")
    return features_embedded

# import pandas as pd
# import numpy as np
# import matplotlib.pyplot as plt
# %matplotlib inline 
# from sklearn.cluster import KMeans
# from sklearn import datasets
# distortions = []
# K = range(1,10)
# for k in K:
#     kmeanModel = KMeans(n_clusters=k)
#     kmeanModel.fit(df)
#     distortions.append(kmeanModel.inertia_)
# Plotting the distortions of K-Means
# plt.figure(figsize=(16,8))
# plt.plot(K, distortions, 'bx-')
# plt.xlabel('k')
# plt.ylabel('Distortion')
# plt.title('The Elbow Method showing the optimal k')
# plt.show()
def elbow_kmeans(datapoints, p):
    from sklearn.cluster import KMeans
    wcss = [] 
    for i in range(2500, 5000, 200): 
        kmeans = KMeans(n_clusters = i, init = 'k-means++', random_state = 42)
        kmeans.fit(datapoints) 
        wcss.append(kmeans.inertia_)
    plt.plot(range(2500, 5000, 200), wcss)
    plt.xlabel('Number of clusters')
    plt.ylabel('WCSS') 
    plt.savefig(f'{os.path.dirname(p)}/elbow_plot.png')

def w2vec_test(word_list, path, title='Word embeddings projected'):

    word_vpath = '/is/cluster/nathanasiou/data/GoogleNews-vectors-negative300.bin'
    kv_model = KeyedVectors.load_word2vec_format(word_vpath, binary=True)

    word_emb = np.zeros((len(word_list), 300))
    import nltk
    from nltk.corpus import stopwords
    cachedStopWords = stopwords.words("english")
    oovs = []
    for i, w in enumerate(word_list):
        words_of_phrase = w.strip().split()
        tmp = np.zeros((300))
        no_of_wds = 0

        for sw in words_of_phrase:
            if sw not in cachedStopWords:
                no_of_wds += 1
                if not kv_model.has_index_for(sw):
                    oovs.append(sw)
                    continue
                tmp += kv_model.get_vector(sw)
        word_emb[i] = tmp / no_of_wds
    print('OOV labels:', oovs)
    word_emb2d = pca_tsne(word_emb)
    source = ColumnDataSource(data=dict(dim1=word_emb2d[:, 0],
                                        dim2=word_emb2d[:, 1],
                                        # color=['red']*32 +['green']*19,
                                        words=word_list))
    ttootps = [("index", "$index"),
               ("(x,y)", "($x, $y)"),
               ("desc", "@words"),]
    p = figure(title='Word embeddings projected', tools=BOKEH_TOOLS,  
               tooltips=ttootps,
               plot_width=1800, plot_height=1200)
            #    x_range=Range1d(140, 275))
    p.scatter(x='dim1', y='dim2', size=8, source=source)#, color='color')
    # p.xaxis[0].axis_label = 'Weight (lbs)'
    # p.yaxis[0].axis_label = 'Height (in)'

    # labels = LabelSet(x='dim1', y='dim2', text='words',
    #                   x_offset=5, y_offset=5, source=source,
    #                   render_mode='canvas')

    # citation = Label(x=70, y=70, x_units='screen', y_units='screen',
    #                  text='Collected by Luke C. 2016-04-01', render_mode='css',
    #                  border_line_color='black', border_line_alpha=1.0,
    #                  background_fill_color='white', background_fill_alpha=1.0)

    # p.add_layout(labels)

    os.makedirs(os.path.dirname(path), exist_ok=True)
    save_bokeh_plot(p, path)
