from pathlib import Path
import json
import os

def get_split_keyids(path: str, split: str):
    assert split in SPLITS
    filepath = Path(path) / split
    with filepath.open("r") as file_split:
        keyids = file_split.readlines()

    keyids = [key.strip() for key in keyids]
    return keyids
SPLITS = ["train", "val", "test", "all", "subset"]


datapath = Path('/is/cluster/work/nathanasiou/data/motion-language/kit')
splitpath = '/is/cluster/work/nathanasiou/data/motion-language/kit-splits'
import spacy
nlp = spacy.load("en_core_web_trf") # en_core_web_sm
# from transformers import BertTokenizer, 
from transformers import DistilBertTokenizer, DistilBertModel
tz_dbert = DistilBertTokenizer.from_pretrained("distilbert-base-uncased")
pos_keep = ['VERB', 'ADJ', 'ADV', 'NOUN']
full_vocab_db = []
full_vocab = []
from tqdm import tqdm
from utils import read_json, write_json
processed_sents = []
for split in ['train', 'val', 'test']:
    keyids = get_split_keyids(path=splitpath, split=split)
    # For SpaCy
    vocab = []
    w_len = []

    # for DistillBERT
    vocab_distillbert = []
    w_len_db = []
    
    for i, keyid in enumerate(tqdm(keyids)):

        metapath = datapath / (keyid + "_meta.json")
        metadata = json.load(metapath.open())
        if metadata["nb_annotations"] == 0:
            print(f"{keyid} has no annotations")
            continue

        annpath = datapath / (keyid + "_annotations.json")
        anndata = json.load(annpath.open())

        assert len(anndata) == metadata["nb_annotations"]

        text_ann = anndata[0]

        bert_tokens = tz_dbert.tokenize(text_ann)
        w_len_db += [len(bert_tokens)]
        # vocab_distillbert.extend(bert_tokens)

        for token in bert_tokens:
            tkn = nlp(token)
            for i in tkn: 
                if i.pos_ in pos_keep:
                    vocab_distillbert.append(i.text)

        proc_sent = nlp(text_ann)
        processed_sents.append(proc_sent)
        w_len += [len(proc_sent)]
        for token in proc_sent:
            if token.pos_ in pos_keep:
                vocab.append(token.text)

    full_vocab_db.extend(vocab)
    full_vocab.extend(vocab_distillbert)
import pickle
with open('nlp-kit.pkl', 'wb') as handle:
    pickle.dump(processed_sents, handle, protocol=pickle.HIGHEST_PROTOCOL)

from collections import Counter

counts_db = dict(Counter(full_vocab_db))
freqs_db = sorted(counts_db.items(), key=lambda pair: pair[1], reverse=True)
counts = dict(Counter(full_vocab))
freqs = sorted(counts.items(), key=lambda pair: pair[1], reverse=True)
write_json(list(set(full_vocab_db)), './vocab-kit-bert.json')
write_json(list(set(full_vocab)), './vocab-kit-spacy.json')
write_json(freqs, './counts-spacy.json')
write_json(freqs_db, './counts-bert.json')
from statistics import mean, median, variance, stdev

print(f'μ:{round(mean(w_len), 2)} +/- {round(variance(w_len), 2)} Median: {round(median(w_len), 2)}')
print(f'μ:{round(mean(w_len_db), 2)} +/- {round(variance(w_len_db), 2)} Median: {round(median(w_len_db), 2)}')

