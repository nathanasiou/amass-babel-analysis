import argparse
from loguru import logger
import glob
import joblib
from tqdm import tqdm
from plot_utils import make_hist_bokeh
from utils import read_json, write_json, fix_spell
import numpy as np
from collections import OrderedDict
'''
RUN EXAMPLE
python divotion/dataset/add_babel_labels.py \
--input-path /is/cluster/nathanasiou/data/amass/processed_amass_smplh_wshape_30fps \ 
--out-path /is/cluster/nathanasiou/data/babel/babel-smplh30fps-gender \
--babel-path /is/cluster/nathanasiou/data/babel/babel_v2.1/
'''

def process_data(input_dir, out_dir):

    shapes_dict = OrderedDict()
    amass_subsets = glob.glob(f'{input_dir}/*/*')
    no_betas = 3    
    for am_s_path in amass_subsets:
        amass_subset = joblib.load(am_s_path)
        for sample in tqdm(amass_subset):            
            shapes_dict[sample['fname']] = sample['betas'][:no_betas]

    shapes = np.array(list(shapes_dict.values()))
    hist_edges = []
    for i in range(shapes.shape[1]):

        hist, edges = np.histogram(shapes[:, i], bins='auto')
        hist_edges.append(edges)
        make_hist_bokeh(title=f'Histogram-Shape Distribution(AMASS) comp: {i}',
                        hist=hist, edges=edges,
                        xaxis='# Samples AMASS',
                        yaxis=f'counts',
                        save_path=f'{out_dir}/shapes_{i}th_component.png')
    
        if i == 1:
            make_hist_bokeh(title=f'Histogram-Shape Distribution(AMASS) comp: {i}',
                        hist=hist, edges=edges,
                        xaxis='# Samples AMASS',
                        yaxis=f'counts',
                        save_path=f'{out_dir}/shapes_{i}th_component.html')

    l1, l2, l3 = [], [], []
    t1, t2 = -1.6, 1.8
    fst_comp = hist_edges[0]
    for k, v in shapes_dict.items():
        if v[0] < t1:
           l1.append(k) 

        elif v[0] <= t2 and v[0] >= t1 :
           l2.append(k) 

        elif v[0] > t2:
           l3.append(k)
    import random
    l =  random.choices(l1, k=5) + random.choices(l2, k=5) + random.choices(l3, k=5)  
    print(l)
if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--input-path', required=True, type=str,
                        help='input path of AMASS data in unzipped format without anything else.')
    parser.add_argument('--out-path', required=True, type=str,
                        help='input path of AMASS data in unzipped format without anything else.')


    args = parser.parse_args()
    input_dir = args.input_path
    out_dir = args.out_path
    logger.info(f'Input arguments: \n {args}')

    db = process_data(input_dir, out_dir)
